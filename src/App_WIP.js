import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import "normalize.css/normalize.css";
import "@blueprintjs/core/dist/blueprint.css";
import * as Blueprint from "@blueprintjs/core";
import * as BlueprintLabs from "@blueprintjs/labs";

const CountryMultiSelect = BlueprintLabs.MultiSelect();

class App extends Component {
  state = {
      orderSize: 5,
      genderValue: "1",
      s1IsOpen: true,
      s2IsOpen: true,
      s3IsOpen: true,
      s4IsOpen: true,
      s5IsOpen: true,
      s6IsOpen: true,
      s7IsOpen: true
  };

  render() {
    return (
      <div className="App pt-ui-text">
        <div className="OrderForm">
          <section className="OrderSection">
            <header>
              <h5>How many people do you need?</h5>
              <input className="pt-input pt-large" type="text" placeholder="Text input" dir="auto" value={this.state.orderSize} />
            </header>
            <Blueprint.Slider 
              value={0}
              onChange={this.getChangeHandler("orderSize")}
              value={this.state.orderSize}
              min={0}
              max={1000} 
              labelStepSize={100} 
              className="pt-intent-primary" 
            />
          </section>
          <section className="OrderSection">
            <header>
              <h6 onClick={this.toggleSection8} >
                Country 
                {
                  this.state.s8IsOpen ? 
                    <Blueprint.Icon iconName="pt-icon-caret-up" /> 
                    : 
                    <Blueprint.Icon iconName="pt-icon-caret-down" />
                }
              </h6>
            </header>
            { this.state.s8IsOpen ? 
              <CountryMultiSelect
                {...flags}
                items={COUNTRIES}
                itemPredicate={this.filterCountry}
                itemRenderer={this.renderCountry}
                noResults={<MenuItem disabled text="No results." />}
                onItemSelect={this.handleCountrySelect}
                popoverProps={{ popoverClassName: popoverMinimal ? Classes.MINIMAL : "" }}
                tagRenderer={this.renderTag}
                tagInputProps={{ tagProps: getTagProps, onRemove: this.handleTagRemove }}
                selectedItems={this.state.countries}
              />
              : 
              "Any"
            }
          </section>
          <section className="OrderSection">
            <header>
              <h6 onClick={this.toggleSection9} >
                Gender 
                {
                  this.state.s9IsOpen ? 
                    <Blueprint.Icon iconName="pt-icon-caret-up" /> 
                    : 
                    <Blueprint.Icon iconName="pt-icon-caret-down" />
                }
              </h6>
            </header>
            { this.state.s9IsOpen ? 
              <Blueprint.RadioGroup
                onChange={this.getChangeHandler("genderValue")}
                selectedValue={this.state.genderValue}
              >
                <Blueprint.Radio label="Any" value="1" />
                <Blueprint.Radio label="Males only" value="2" />
                <Blueprint.Radio label="Females only" value="3" />
              </Blueprint.RadioGroup>
              : 
              "Any"
            }
          </section>
          <section className="OrderSection">
            <header>
              <h6 onClick={this.toggleSection7} >
                Age range 
                {
                  this.state.s7IsOpen ? 
                    <Blueprint.Icon iconName="pt-icon-caret-up" /> 
                    : 
                    <Blueprint.Icon iconName="pt-icon-caret-down" />
                }
              </h6>
            </header>
            { this.state.s7IsOpen ? 
              <Blueprint.RangeSlider
                value={[18, 100]}
                min={0} 
                max={100}
                labelStepSize={10} 
                className="pt-intent-primary"
              />
              : 
              "Any"
            }
          </section>
          <section className="OrderSection">
            <header>
              <h6 onClick={this.toggleSection2} >
                Max education level 
                {this.state.s2IsOpen ? 
                  <Blueprint.Icon iconName="pt-icon-caret-up" /> 
                  : 
                  <Blueprint.Icon iconName="pt-icon-caret-down" />
                }
              </h6>
            </header>
            { this.state.s2IsOpen ?
              <div>
                <Blueprint.Checkbox label="Any" checked value="1" />
                <Blueprint.Checkbox label="Primary school" value="2" />
                <Blueprint.Checkbox label="Some highschool" value="3" />
                <Blueprint.Checkbox label="Highschool graduate" value="4" />
                <Blueprint.Checkbox label="Some college" value="5" />
                <Blueprint.Checkbox label="College graduate" value="6" />
                <Blueprint.Checkbox label="Some post-grad" value="7" />
                <Blueprint.Checkbox label="Post graduate" value="8" />
              </div>
              : 
              "Any"
            }
          </section>
          <section className="OrderSection">
            <header>
              <h6 onClick={this.toggleSection3} >
                Employment Status 
                {this.state.s3IsOpen ? 
                  <Blueprint.Icon iconName="pt-icon-caret-up" /> 
                  : 
                  <Blueprint.Icon iconName="pt-icon-caret-down" />
                }
              </h6>
            </header>
            { this.state.s3IsOpen ? 
              <div>
                <Blueprint.Checkbox label="Any" checked value="1" />
                <Blueprint.Checkbox label="Seeking employment" value="2" />
                <Blueprint.Checkbox label="Not seeking employment" value="3" />
                <Blueprint.Checkbox label="Part-time" value="4" />
                <Blueprint.Checkbox label="Full-time" value="5" />
                <Blueprint.Checkbox label="Self-employed" value="6" />
                <Blueprint.Checkbox label="Retired" value="7" />
              </div>
              : 
              "Any"
            }
          </section>
          <section className="OrderSection">
            <header>
              <h6 onClick={this.toggleSection4} >
                Annual household income 
                {this.state.s4IsOpen ? 
                  <Blueprint.Icon iconName="pt-icon-caret-up" /> 
                  : 
                  <Blueprint.Icon iconName="pt-icon-caret-down" />
                }
              </h6>
            </header>
            { this.state.s4IsOpen ? 
              <div>
                <Blueprint.Checkbox label="Any" checked value="1" />
                <Blueprint.Checkbox label="$0 – $10,000" value="2" />
                <Blueprint.Checkbox label="$10,001 – $40,000" value="3" />
                <Blueprint.Checkbox label="$40,001 – $70,000" value="4" />
                <Blueprint.Checkbox label="$70,001 – $100,000" value="5" />
                <Blueprint.Checkbox label="$100,000+" value="6" />
              </div>
              : 
              "Any"
            }
          </section>
          <section className="OrderSection">
            <header>
              <h6 onClick={this.toggleSection5} >
                Hours spent online per day 
                {this.state.s5IsOpen ? 
                  <Blueprint.Icon iconName="pt-icon-caret-up" /> 
                  : 
                  <Blueprint.Icon iconName="pt-icon-caret-down" />
                }
              </h6>
            </header>
            { this.state.s5IsOpen ? 
              <div>
                <Blueprint.Checkbox label="Any" checked value="1" />
                <Blueprint.Checkbox label="0 – 1" value="2" />
                <Blueprint.Checkbox label="1 – 3" value="3" />
                <Blueprint.Checkbox label="3 – 5" value="4" />
                <Blueprint.Checkbox label="5+" value="5" />
              </div>
              : 
              "Any"
            }
          </section>
          <section className="OrderSection">
            <header>
              <h6 onClick={this.toggleSection6} >
                Technical proficiency 
                {this.state.s6IsOpen ? 
                  <Blueprint.Icon iconName="pt-icon-caret-up" /> 
                  : 
                  <Blueprint.Icon iconName="pt-icon-caret-down" />
                }
              </h6>
            </header>
            { this.state.s6IsOpen ? 
              <div>
                <Blueprint.Checkbox label="Any" checked value="1" />
                <Blueprint.Checkbox label="Beginner" value="2" />
                <Blueprint.Checkbox label="Intermediate" value="3" />
                <Blueprint.Checkbox label="Advanced" value="4" />
              </div>
              : 
              "Any"
            }
          </section>
        </div>
      </div>
    );
  }

  toggleSection1 = () => {
    this.setState({ s1IsOpen: !this.state.s1IsOpen });
  }

  toggleSection2 = () => {
    this.setState({ s2IsOpen: !this.state.s2IsOpen });
  }
  
  toggleSection3 = () => {
    this.setState({ s3IsOpen: !this.state.s3IsOpen });
  }
  
  toggleSection4 = () => {
    this.setState({ s4IsOpen: !this.state.s4IsOpen });
  }
  
  toggleSection5 = () => {
    this.setState({ s5IsOpen: !this.state.s5IsOpen });
  }
  
  toggleSection6 = () => {
    this.setState({ s6IsOpen: !this.state.s6IsOpen });
  }
  
  toggleSection7 = () => {
    this.setState({ s7IsOpen: !this.state.s7IsOpen });
  }

  renderTag = (country) => {
    return country.title;
  }

  renderCountry = ({ handleClick, isActive, item: country }) => {
      const classes = classNames({
          [Classes.ACTIVE]: isActive,
          [Classes.INTENT_PRIMARY]: isActive,
      });

      return (
          <MenuItem
            className={classes}
            iconName={this.isCountrySelected(country) ? "tick" : "blank"}
            key={country.name}
            onClick={handleClick}
            text={`${country.title}`}
            shouldDismissPopover={false}
          />
      );
  }

  filterCountry(query, country, index) {
      return `${index + 1}. ${country.title.toLowerCase()} ${country.year}`.indexOf(query.toLowerCase()) >= 0;
  }

  handleTagRemove = (_tag, index) => {
      this.deselectCountry(index);
  }

  getSelectedCountryIndex(country) {
      return this.state.countries.indexOf(country);
  }

  isCountrySelected(country) {
      return this.getSelectedCountryIndex(country) !== -1;
  }

  selectCountry(country) {
      this.setState({ countries: [...this.state.countries, country] });
  }

  deselectCountry(index) {
      this.setState({ countries: this.state.country.filter((_country, i) => i !== index) });
  }

  handleCountrySelect = (country) => {
      if (!this.isCountrySelected(country)) {
          this.selectCountry(country);
      } else {
          this.deselectCountry(this.getSelectedCountryIndex(country));
      }
  }

  getChangeHandler(key) {
    return (value) => this.setState({ [key]: value });
  }
}

export default App;
